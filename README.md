# Butchered-Pig plugin for BEANS

`BEANS` is a web-based software for interactive distributed data analysis with a clear interface for querying, filtering, aggregating, and plotting data from an arbitrary number of datasets and tables [https://gitlab.com/beans-code/beans](https://gitlab.com/beans-code/beans).

`Butchered-Pig` is an attempt to provide implementation for distributed data analysis which works on any machines (even with limited RAM) [https://gitlab.com/ahypki/butchered-pig](https://gitlab.com/ahypki/butchered-pig).

## Features

Currently `Butchered-Pig` supports almost all keywords from `Apache Pig`. 