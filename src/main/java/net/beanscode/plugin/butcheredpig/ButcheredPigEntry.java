package net.beanscode.plugin.butcheredpig;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.padRight;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import butcheredpig.Settings;
import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.ButcheredPigScriptFactory;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationFactory;
import net.beanscode.model.cass.notification.NotificationType;
import net.beanscode.model.connectors.BeansConnector;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.dataset.TableRemoveJob;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryStatus;
import net.beanscode.model.notebook.ReloadPropagator;
import net.beanscode.model.settings.Clipboard;
import net.beanscode.plugin.pig.BeansTable;
import net.beanscode.plugin.pig.PigManager;
import net.beanscode.plugin.pig.PigScript;
import net.beanscode.plugin.pig.PigScriptClearEmptyTablesJob;
import net.beanscode.plugin.pig.PigScriptJob;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.mail.Mailer;
import net.hypki.libs5.weblibs.settings.Setting;
import net.hypki.libs5.weblibs.settings.SettingFactory;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;

public class ButcheredPigEntry extends NotebookEntry {
	
	public static final String META_QUERY 			= "META_QUERY";
	private static final String META_DATAID 		= "META_DATAID";
	private static final String META_READ_TABLES	= "META_READ_TABLES";
	
//	@Expose
//	private String query = null;
	
//	@Expose
//	@NotNull
//	@AssertValid
//	private UUID dataId = null;
		
//	@Expose
//	private List<UUID> readTables = null;
		
	public ButcheredPigEntry() {
		setDataId(UUID.random());
	}
	
	public ButcheredPigEntry(UUID userId, UUID notebookId, String name) {
		setId(UUID.random());
		setUserId(userId);
		setNotebookId(notebookId);
		setName(name);
		setCreate(SimpleDate.now());
		setDataId(UUID.random());
	}
	
	public ButcheredPigEntry(User user, UUID notebookId, String name) {
		setId(UUID.random());
		setUserId(user.getUserId());
		setNotebookId(notebookId);
		setName(name);
		setCreate(SimpleDate.now());
		setDataId(UUID.random());
	}
	
	public ButcheredPigEntry(Notebook notebook, String name) {
		setId(UUID.random());
		setUserId(notebook.getUserId());
		setNotebookId(notebook.getId());
		setName(name);
		setCreate(SimpleDate.now());
		setDataId(UUID.random());
	}
	
	@Override
	public String getShortDescription() {
		return "Butchered Pig script";
	}
	
	@Override
	public String getSummary() {
		String tmp = StringUtilities.substringMax(getQuery(), 0, 100);
		return "Butchered Pig script: " + ((getQuery() != null && getQuery().length() > 100) ? tmp + "..." : tmp);
	}
		
	@Override
	public boolean isReloadNeeded() {
//		List<UUID> oldTablesId = new ArrayList<>();
//		if (getTableStats() != null)
//			for (TableStats tableStats : getTableStats().values())
//				oldTablesId.add(tableStats.getTableId());
		
		Set<UUID> newReadTables = new java.util.HashSet<>();
		
		for (BeansTable uni : PigManager.iterateLoadTables(getId(), getQuery())) {
			for (Table table : uni.iterateTables()) {
				
				if (table.getColumnDefs() == null || table.getColumnDefs().size() == 0) {
					LibsLogger.warn(ButcheredPigEntry.class, "Table " + table.getId() + " does not have ColumnDefs specified, ignoring it from determining "
							+ "whether the Pig script should be reloaded");
					continue;
				}
					
				// check for new tables
				if (!getReadTables().contains(table.getId()))
					return true;
				
//				try {
//					final TableStats oldTableStats = getTableStats().get(table.getId());
//					if (oldTableStats == null 
//							|| !oldTableStats.equals(table.getTableStats()))
//						return true;
//				} catch (IOException e) {
//					LibsLogger.error(PigScript.class, "Cannot check if table " + table.getId() + " needs reload, skipping...", e);
//				}
				
				newReadTables.add(table.getId());
//				oldTablesId.remove(table.getId());
			}
		}

		// check if the list of tables has changed
		if (getReadTables().size() != newReadTables.size())
			return true;
		
		return false;
	}
	
	@Override
	public boolean isRunning() throws ValidationException, IOException {
		Notification n = NotificationFactory.getNotification(getId());

		if (n != null)
			return n.getNotificationType() != NotificationType.OK
				&& n.getNotificationType() != NotificationType.ERROR;
		else
			return true;
	}
	
	@Override
	public ReloadPropagator reload() throws ValidationException, IOException {
		runQuery(true);
		return ReloadPropagator.BLOCK;
	}
	
	@Override
	public void additionalValidation() throws ValidationException {
		super.additionalValidation();
	}
	
	public static ButcheredPigEntry getButcheredPigEntry(String queryId) throws IOException {
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, queryId)), 
				DbObject.COLUMN_DATA, 
				ButcheredPigEntry.class);
	}
	
	public static ButcheredPigEntry getButcheredPigEntry(UUID queryId) throws IOException {
		return queryId != null ? getButcheredPigEntry(queryId.getId()) : null;
	}
	
	@Override
	public MutationList getRemoveMutations() {
		return super.getRemoveMutations()
				.addInsertMutations(new ButcheredPigScriptRemoveDependenciesJob(this));
	}
	
	public String getQuery() {
		return getMetaAsString(META_QUERY, null);
	}

	public ButcheredPigEntry setQuery(String query) {
		setMeta(META_QUERY, query);
		
//		updateTableStats();
		
		return this;
	}
	
	private void updateReadTables() throws ValidationException, IOException {
		getReadTables().clear();
		for (BeansTable uni : PigManager.iterateLoadTables(getId(), getQuery())) {
			for (Table table : uni.iterateTables()) {
				if (!getReadTables().contains(table.getId()))
					getReadTables().add(table.getId());
			}
		}
		save();
	}

	/**
	 * Runs by default in background
	 * 
	 * @throws IOException
	 * @throws ValidationException
	 */
	public void runQuery() throws IOException, ValidationException {
		runQuery(true);
	}

	public void runQuery(boolean inBackground) throws IOException, ValidationException {
		assertTrue(notEmpty(getQuery()), "Query script is empty for the query: " + getName());
		
		Watch w = new Watch();
		
		new Notification()
			.setId(getId())
			.setDescription(getName())
			.setLocked(true)
			.setNotificationType(NotificationType.INFO)
			.setTitle("Pig script submitted")
			.setUserId(getUserId())
			.save();
		
		String errMsg = null;
		String validationSummary = "";
		
		if (inBackground)
			new ButcheredPigEntryJob(getId()).save();
		else {
			try {
				updateReadTables();
				
				clear();
				Clipboard.remove(getId().getId() + "*");
				
				save();
				
				// remove previous NotebookTable
				TableRemoveJob toRemoveJob = new TableRemoveJob(getUserId());
				for (Table nt : iterateNotebookTables())
					toRemoveJob.getTablesToRemove().add(nt.getId());
				if (toRemoveJob.getTablesToRemove().size() > 0)
					toRemoveJob.save();
				
	//			new QueryStatus(this, PigScriptStatus.RUNNING, "Query started").save();
				new Notification()
					.setId(getId())
					.setDescription(getName())
					.setLocked(true)
					.setNotificationType(NotificationType.INFO)
					.setTitle("Pig script running")
					.setUserId(getUserId())
					.save();
			
				// prepare params for pig
				HashMap<String, String> pigParams = new HashMap<>();
				for (Meta meta : getNotebook().getMeta())
					pigParams.put(meta.getName(), meta.getValue().toString());
				
				// new dataId for Query, so that depending data can be removed in the background and there will be no collision
				setDataId(UUID.random());
				
				// run query
				ButcheredPigScript bp = new ButcheredPigScript();
				bp.setId(getId());
				bp.setNotebookId(getNotebookId());
				bp.setUserId(getUserId());
				bp.setToParse(getQuery());
				bp
					.getContext()
					.setWorkingDir(BeansSettings.getPluginDefaultFolder());
				bp.save();
				
				bp.run();
				
				while (true) {
					bp = ButcheredPigScriptFactory.getButcheredPigScript(bp.getId());
					
					if (bp.getProgress().isDone() || bp.getProgress().isFailed()) {
						break;
					} else {
						try {//new net.beanscode.model.scripts.butcheredpig.ButcheredPigScriptJob(bp).save();
							Thread.sleep(Settings.SCRIPT_BUTCHEREDPIG_SLEEP_MS);
						} catch (Throwable e) {
							LibsLogger.error(ButcheredPigEntry.class, "Cannot sleep waiting for ButcheredPigEntry", e);
						}
					}
				}
				
				// removing failed connectors, optimizing table
				try {
					Watch optimizeTab = new Watch();
					for (Table table : iterateNotebookTables()) {
						// to get rid for now of "__" tables names
						table.clearNonCommittedConnectors(); 
						
						// reloading rights to the new tables
						table.reloadPermissions(true);
						
						// adding this table to the search indices of the user who can read/write this table
						table.index();
						
						// TODO optimize conf
//						table.optimize(false);
					}
					LibsLogger.debug(PigScript.class, "Tables optimized in " + optimizeTab.toString());
				} catch (Throwable t) {
					LibsLogger.error(PigScript.class, "Cannot clear non-commited connectors", t);
				}
				
				// setting status for this entry
				if (bp.getProgress().isFailed()) {
					setStatus(NotebookEntryStatus.FAIL);
					errMsg = "Butchered pig failed";
					save();
					
					new Notification()
						.setId(getId())
						.setDescription(getName())
						.setLocked(false)
						.setNotificationType(NotificationType.ERROR)
						.setTitle("Butchered Pig script failed")
						.setUserId(getUserId())
						.save();
				} else {
					new Notification()
						.setId(getId())
						.setDescription(getName())
						.setLocked(false)
						.setNotificationType(NotificationType.OK)
						.setTitle("Pig script done")
						.setUserId(getUserId())
						.save();
				}
				
				// clear (possible) empty tables for this query
				clearEmptyTables(true);
				
				LibsLogger.debug(PigScriptJob.class, "Query finished successfully");
			} catch (Throwable e) {
				LibsLogger.error(PigScriptJob.class, "Pig script failed, consuming job...", e);
				
				errMsg = e.getMessage();
				if (nullOrEmpty(errMsg))
					errMsg = "Script failed, detailed cause is unknown";
				
				try {					
//					new QueryStatus(this, PigScriptStatus.FAILED, 100, errMsg).save();
					new Notification()
						.setId(getId())
						.setDescription("ERROR: " + errMsg)//getName())
						.setLocked(false)
						.setNotificationType(NotificationType.ERROR)
						.setTitle("Pig script failed")
						.setUserId(getUserId())
						.save();
				} catch (Throwable t) {
					LibsLogger.error(PigScriptJob.class, "Cannot save " + Notification.class.getSimpleName(), t);
				}
				
			} finally {
				
			}
			
			// check if one has to notify through the email
			try {
				// TODO switch this if off - notify always
				Setting setting = SettingFactory.getSetting(getUserId(), BeansSettings.SETTING_NOTIFICATION_MAIL);
				if (setting != null) {
					if ((System.currentTimeMillis() - w.getStart()) > setting.getValue("minimum").getValueAsInteger()) {
						boolean isSuccess = nullOrEmpty(errMsg);
						
						StringBuilder sb = new StringBuilder();
						sb.append(padRight("Apache Pig script: ", 20) + getName() + "\n");
						sb.append(padRight("Notebook: ", 20) + getNotebook().getName() + "\n");
						sb.append(padRight("Duration: ", 20) + w + "\n");
						sb.append(padRight("Started at: ", 20) + w.getStartDate().toStringHuman() + "\n");
						sb.append(padRight("Finished at: ", 20) + SimpleDate.now().toStringHuman() + "\n");
						
						if (notEmpty(validationSummary))
							sb.append(validationSummary);
						
						if (!isSuccess) {
							sb.append("\nError message: \n");
							sb.append(errMsg + "\n");
						}
						
						Mailer.sendMail(BeansSettings.getMailCredentials(), 
								UserFactory.getUser(getUserId()).getEmail().toString(), 
								null, 
								null, 
								"[BEANS] " + (isSuccess ? "SUCCESSFULL" : "FAILED") + " " + getName(), 
								sb.toString(), 
								false, 
								null);
					}
				}
			} catch (Throwable e) {
				LibsLogger.error(PigScript.class, "Cannot send notification email", e);
			}
		}
	}

	private void clear() throws IOException {
		for (Table tab : iterateNotebookTables()) {
			for (BeansConnector beansConn : tab.iterateBeansConnectors()) {				
				beansConn.getConnector().clear();
			}
		}
	}

	
	public void clearEmptyTables(boolean inBackground) throws ValidationException, IOException {
		try {
			// TODO do nothing yet
			if (true)
				return;
			
			
			if (inBackground) {
				new PigScriptClearEmptyTablesJob(getId()).save();
				return;
			}
			
			for (Table table : iterateNotebookTables()) {
				Boolean removeTable = null;
				
				try {
					for (Row row : table.getConnectorList().iterateRows(null)) {
						if (row != null)
							removeTable = false;
						else
							removeTable = true;
						break;
					}
					
					if (removeTable == null)
						removeTable = true;
				} catch (Throwable t) {
					LibsLogger.error(PigScript.class, "Cannot iterate rows for Pig script " + getId() 
							+ " and table " + table.getId() + ", not removing this table", t);
				}
				
				if (removeTable != null
						&& removeTable) {
					LibsLogger.debug(PigScript.class, "Removing empty Table " + table.getId());
					table.remove();
				} else {
					// TODO DEBUG sometimes table is not indexed..
					try {
						table.index();
					} catch (Exception e) {
						LibsLogger.error(PigScript.class, "Cannot reindex table", e);
					}				
				}
			}
			
			LibsLogger.debug(PigScript.class, "Empty tables cleared for Pig script " + getId());
		} catch (Throwable e) {
			LibsLogger.error(PigScript.class, "Cannot clear empty tables for PigScript " + getId(), e);
		}
	}

	public UUID getDataId() {
		String tmp = getMetaAsString(META_DATAID, null);
		return tmp != null ? new UUID(tmp) : null;
	}

	public void setDataId(UUID dataId) {
		setMeta(META_DATAID, dataId != null ? dataId.getId() : null);
	}
	
	public Iterable<Table> iterateNotebookTables() throws IOException {
		return TableFactory.iterateTables(getNotebookId(), getId());//this);
	}
	
	@Override
	public String getEditorClass() {
		return "net.beanscode.web.view.notebook.ButcheredPigPanel";
	}

	private List<UUID> getReadTables() {
		List<UUID> readTables = null;
		String tbStr = getMetaAsString(META_READ_TABLES, null);
		if (tbStr == null) {
			readTables = new ArrayList<>();
		} else {
			readTables = JsonUtils.readList(tbStr, UUID.class);
		}
		return readTables;
	}

	private void setReadTables(List<UUID> readTables) {
		setMeta(META_READ_TABLES, JsonUtils.objectToStringPretty(readTables));
	}
	
	@Override
	public NotebookEntry save() throws ValidationException, IOException {
		return super.save();
	}
	
	@Override
	public void stop() throws ValidationException, IOException {
//		try {
//			if (getPigMode().equals("tez")
//					|| getPigMode().equals("mapreduce")) {
//				// stopping the job using yarn command
//				for (HadoopJob hadoopJob : HadoopManager.getApplicationsList()) {
//					if (hadoopJob.getApplicationName().contains("beans-" + getId())
//							&& hadoopJob.getState() == HadoopJobState.RUNNING) {
//						LibsLogger.info(PigScript.class, "Stopping Hadoop job " + hadoopJob);
//						HadoopManager.hadoopStop(hadoopJob);
//					}
//				}
//			} else {
//				// removing Job, stopping Thread which is calculating this Job
//				
//				// looking for a proper working Thread
//				String jobKey = null;
//				for (JobsWorker worker : JobsManager.getJobsWorkersThreads()) {
//					if (worker.getCurrentJob() != null
//							&& worker.getCurrentJob() instanceof PigScriptJob) {
//						PigScriptJob pigJob = (PigScriptJob) worker.getCurrentJob();
//						if (pigJob.getPigScript().getId().equals(getId())) {
//							jobKey = pigJob.getKey();
//							break;
//						}
//					} 
//				}
//				
//				if (jobKey != null) {
//					if (!JobsManager.stop(jobKey)) {
//						LibsLogger.error(PigScript.class, "Cannot stop PigScriptJob for PigScript " + getId());
//					}
//				}
//			}
//		} catch (IOException e) {
//			LibsLogger.error(PigScript.class, "Stopping pig script failed", e);
//		}
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}
	
}
