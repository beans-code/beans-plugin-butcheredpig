package net.beanscode.plugin.butcheredpig;

import static net.hypki.libs5.weblibs.user.UserFactory.getUser;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansChannels;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.users.BeansMailJob;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.settings.SettingFactory;
import net.hypki.libs5.weblibs.settings.SettingValue;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class ButcheredPigEntryJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID queryId = null;
	
	private ButcheredPigEntry butcheredPigScript = null;

	public ButcheredPigEntryJob() {
		super(BeansChannels.CHANNEL_LONG_NORMAL);
	}
	
	public ButcheredPigEntryJob(UUID queryId) {
		super(BeansChannels.CHANNEL_LONG_NORMAL);
		setQueryId(queryId);
	}
	
	@Override
	protected String getLogSeparately() {
		try {
			SettingValue pigPath = SettingFactory.getSettingValue(BeansSettings.SETTING_PIG_FOLDER, "path");
			FileExt f = new FileExt(pigPath.getValueAsString() + "/beans-" + getPigScript().getNotebookId() + ".log");
			return f.getAbsolutePath();
		} catch (IOException e) {
			LibsLogger.error(ButcheredPigEntryJob.class, "Cannot log separatelly Job", e);
			return super.getLogSeparately();
		}
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (getPigScript() == null) {
			LibsLogger.debug(ButcheredPigEntryJob.class, "Pig script ", getQueryId(), " does not exist, consuming job..");
			return;
		}
		
		// if pig is switched off then postpone
		if (!BeansSettings.getSettingAsBoolean("PIG_ENABLED", "enabled", true)) {
			LibsLogger.info(ButcheredPigEntryJob.class, "Apache Pig scripts are switched off, postponing");
			setPostponeMs(5 * DateUtils.ONE_MINUTE_MS);
			return;
		}
		
		try {
			Watch watch = new Watch();
			
			getPigScript().runQuery(false);
			
			if (watch.mins() > 5.0) {
				try {
					User user = getUser(getPigScript().getUserId());
					new BeansMailJob(user.getEmail(), 
							"[Beans] Apache Pig query '" + getPigScript().getName() + "' finished", 
							"[Beans] Apache Pig query '" + getPigScript().getName() + "' finished in " + watch);
				} catch (Throwable e) {
					LibsLogger.error(ButcheredPigEntryJob.class, "Cannot sent email notification", e);
				}
			}
		} catch (Throwable e) {
			LibsLogger.error(ButcheredPigEntryJob.class, "Pig script job failed, consuming job", e);
		}
	}
	
	@Override
	public String getDescription() {
		return "Pig script: " + getPigScript().getName();
	}

	public UUID getQueryId() {
		return queryId;
	}

	public void setQueryId(UUID queryId) {
		this.queryId = queryId;
	}

	public ButcheredPigEntry getPigScript() {
		if (butcheredPigScript == null) {
			try {
				butcheredPigScript = ButcheredPigEntry.getButcheredPigEntry(getQueryId());
			} catch (IOException e) {
				LibsLogger.error(ButcheredPigEntryJob.class, "Cannot read Pig script from DB", e);
			}
		}
		return butcheredPigScript;
	}

	private void setPigScript(ButcheredPigEntry pigScript) {
		this.butcheredPigScript = pigScript;
	}
}
