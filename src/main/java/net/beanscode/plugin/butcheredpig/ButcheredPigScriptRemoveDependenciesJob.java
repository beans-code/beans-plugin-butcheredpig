package net.beanscode.plugin.butcheredpig;

import java.io.IOException;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.dataset.Table;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.settings.SettingFactory;
import net.hypki.libs5.weblibs.settings.SettingValue;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class ButcheredPigScriptRemoveDependenciesJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private String pigScript = null;
	
	public ButcheredPigScriptRemoveDependenciesJob() {
		
	}
	
	public ButcheredPigScriptRemoveDependenciesJob(ButcheredPigEntry pigScript) {
		setPigScript(pigScript);
	}

	@Override
	public void run() throws IOException, ValidationException {
		ButcheredPigEntry pig = getPigScript();
		
		try {
			SearchManager.removeObject(pig.getCombinedKey(), pig.getColumnFamily());
		} catch (Exception e) {
			LibsLogger.error(ButcheredPigScriptRemoveDependenciesJob.class, "Index not found, consuming error..", e);
		}
		
		// remove query status if any exists (and QueryStatusSorted)
//		QueryStatus qs = QueryStatus.getStatus(pig.getId());
//		if (qs != null)
//			qs.remove();
		
		// remove NotebookTables
		for (Table nt : pig.iterateNotebookTables()) {
			try {
				nt.remove();
			} catch (Exception e) {
				LibsLogger.error(ButcheredPigScriptRemoveDependenciesJob.class, "Index not found, consuming error..", e);
			}
		}
		
		// remove pig script
		SettingValue pigPath = SettingFactory.getSettingValue(BeansSettings.SETTING_PIG_FOLDER, "path");
		FileExt f = new FileExt(pigPath.getValueAsString() + "/beans-" + pig.getId().getId() + ".pig");
		if (f.exists()) {
			f.delete();
			LibsLogger.debug(ButcheredPigScriptRemoveDependenciesJob.class, "File ", f.getAbsolutePath(), " removed");
		}
		
		// remove pig log file
		f = new FileExt(pigPath.getValueAsString() + "/beans-" + pig.getId().getId() + ".log");
		if (f.exists()) {
			f.delete();
			LibsLogger.debug(ButcheredPigScriptRemoveDependenciesJob.class, "File ", f.getAbsolutePath(), " removed");
		}
	}

	public ButcheredPigEntry getPigScript() {
		return JsonUtils.fromJson(pigScript, ButcheredPigEntry.class);
	}

	public void setPigScript(ButcheredPigEntry pigScript) {
		this.pigScript = pigScript.getData();
	}
}
